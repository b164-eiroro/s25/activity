// $project
db.fruits.aggregate([
    { $unwind: "$origin" },
    { $group: {_id: "$origin", fruits: { $sum: 1 }}}
])


// $count
db.fruits.aggregate([
    { $match: {onSale: true}},
    { $count: "totalFruitsOnSale" }
])

// $gte
db.fruits.aggregate([
    { $match: {stock: {$gte : 20}}},
    { $count: "totalHighStocks" }
])

// $avg
db.fruits.aggregate([
    { $match: {onSale: true}},
    { $group: {_id: "$supplier_id", averagePrice: {$avg: "$price" }}}
])

// $max
db.fruits.aggregate([
    { $group: {_id: "$supplier_id", highestPrice: {$max: "$price" }}}
])

// $min
db.fruits.aggregate([
    { $group: {_id: "$supplier_id", lowestPrice: {$min: "$price" }}}
])